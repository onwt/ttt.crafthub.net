+++
+++

![CraftHub TTT Logo](TTT.png)
# Welcome to our Crafthub TTT2 server. 
## Enjoy your stay!

Visit our website for other servers and discord link: CraftHub.net

## What is TTT2?

TTT2 is an extension of classic TTT thats includes a host of improvements and the addition of Roles.  

Full Role list below

!role - Will give you a decription of your current role

# Server Rules
## 0. Have Fun
* We play to have fun. If your actions are actively impeding everyone elses ability to have fun, you should reconsider them.

## 1. Do Not RDM
* RDM stands for Random Deathmatch. It means killing someone for a random or no reason. Further:
* Do not kill AFKs.
* Do not PropKill (killing people using in map objects like chairs).
* Do not instruct others to kill someone for:
  * following you.
  * because they won't test.
  * a specific player model/hat/trail.

## 2. Do Not Ghost/Metagame
* Ghosting or Metagaming includes giving a living player information regarding the current round while dead.

## 3. Do Not Hack, Cheat, or Script
* Hacking, injecting, cheating or scripting will result in an immediate permanent ban.

## 4. Do Not Traitor/Karma Bait
* Don't act like a traitor if you're innocent in order for someone else to lose karma.

## 5. Do Not Engage In Hateful Conduct
* Racism.
* Homophobia.
* Sexual Harassment.
* Prejudiced Behaviour.
* Any other form of disrespect directed towards another player.
* Offensive usernames.

## 6. Do Not Spam
* Spamming includes using microphone, text, or radio chat commands in a manner that annoys other players.

## 7. Do Not Use Unreadable Usernames
* Names should be readable and pronounceable.

## 8. Do Not Use NSFW Avatars/Sprays
* If it would get you banned off Twitch, it isn't allowed here.

# Role list

## Innocents
 - Innocent: You are an innocent traitor. Try to not get killed and complete any map objectives!
 - Survivalist: You can buy stuff from the Buy Menu (Default: 'C'). You get more credits for every Traitor you kill.
 - Trapper: You can activate Traitor buttons (red hands floating in the map).
 - Priest: You can use your Holy Deagle to invite Innocents into your Brotherhood. Be careful, you will die if you invite a Traitor/Killer!
 - Spectre: Whoever kills you will be surrounded by black smoke. If they are killed, you will be brought back to life.

## Detective
 - Detective: You can buy stuff from the Buy Menu (Default: 'C'). Use your items and DNA scanner to find the Traitors/Killers.
 - Sheriff and Deputy: You can buy stuff from the Buy Menu (Default: 'C'). As Sheriff, you can use your Deputy Deagle to make someone a Deputy, who can also buy stuff. If one of you dies, you both die.

## Traitor
 - Traitor: You can buy stuff from the Buy Menu (Default: 'C'). Make sure only Traitors are alive at the end of the round!
 - Executioner: You are assigned a target at the bottom left of your screen. You do double damage to your target.
 - Mesmerist: You can use your Role Defibulator to revive an Innocent and turn them into a Traitor.

## Neutral Killers
 - Jackal and Sidekick: You can buy stuff from the Buy Menu (Default: 'C'). As Jackal, you can use your Sidekick Deagle to make someone a Sidekick, who can also buy stuff. If you die, they become Jackal.
 - Serial Killer: You can buy stuff from the Buy Menu (Default: 'C'). You can see players through walls. Rip and tear, until it is done.

## Neutral
 - Jester: You do no damage. However, if you can trick someone into killing you, you will win the round!